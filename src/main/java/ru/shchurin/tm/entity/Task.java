package ru.shchurin.tm.entity;

import ru.shchurin.tm.entity.Project;

public class Task {

    private String name;

    private Project project;

    public Task(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
}
