package ru.shchurin.tm;

import ru.shchurin.tm.entity.Project;
import ru.shchurin.tm.entity.Task;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        List<Project> projects = new ArrayList<>();
        List<Task> tasks = new ArrayList<>();

        System.out.println("*** WELCOME TO TASK MANAGER ***");

        Boolean exit = false;

        while (!exit) {

            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String command = reader.readLine();

            if (command.equals("project-clear")) {

                projects.clear();
                System.out.println("[ALL TASK REMOVE]");

            } else if (command.equals("project-create")) {

                System.out.println("[PROJECT CREATE]");
                System.out.println("ENTER NAME:");

                String name = reader.readLine();
                projects.add(new Project(name));

                System.out.println("[OK]");

            } else if (command.equals("project-list")) {

                System.out.println("[PROJECT LIST]");
                for (int i = 1; i <= projects.size(); i++) {
                    System.out.println(i + " " + projects.get(i - 1).getName());
                }

            } else if (command.equals("project-remove")) {

                int index = Integer.parseInt(reader.readLine());
                projects.remove(index);
                System.out.println("[PROJECT REMOVE]");

            } else if (command.equals("task-clear")) {

                tasks.clear();
                System.out.println("[ALL TASK REMOVE]");

            } else if (command.equals("task-create")) {

                System.out.println("[TASK CREATE]");
                System.out.println("ENTER NAME:");

                String name = reader.readLine();
                tasks.add(new Task(name));

                System.out.println("[OK]");

            } else if (command.equals("task-list")) {

                System.out.println("[TASK LIST]");
                for (int i = 1; i <= tasks.size(); i++) {
                    System.out.println(i + " " + tasks.get(i - 1).getName());
                }

            } else if (command.equals("task-remove")) {

                int index = Integer.parseInt(reader.readLine());
                tasks.remove(index);
                System.out.println("[TASK REMOVE]");

            } else if (command.equals("help")) {

                System.out.println("help: Show all commands.");
                System.out.println("project-clear");
                System.out.println("project-create");
                System.out.println("project-list");
                System.out.println("project-remove");
                System.out.println("task-clear");
                System.out.println("task-create");
                System.out.println("task-list");
                System.out.println("task-remove");

            } else if (command.equals("exit")) {

                exit = true;

            } else {
                System.out.println("You entered wrong command");
            }


        }


    }
}
